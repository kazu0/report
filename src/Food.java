import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class Food {
    private JPanel root;
    private JLabel topLabel;
    private JButton ponDeRingButton;
    private JButton oldFashionButton;
    private JButton frenchCrullerButton;
    private JButton strawberryRingButton;
    private JButton goldenChocolateButton;
    private JButton custardCreamButton;
    private JTextPane total0YenTextPane;
    private JButton checkOutButton;
    private JTextPane textPane1;
    private JCheckBox takeOutCheckBox;
    private JTextPane total0YenIncludingTextPane;
    private JTextPane numberOfOrders0TextPane;

    public Food() {
        ponDeRingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pon de Ring");
            }
        });
        oldFashionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Old fashion");
            }
        });
        frenchCrullerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("French cruller");
            }
        });
        strawberryRingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Strawberry ring");
            }
        });
        goldenChocolateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Golden chocolate");
            }
        });
        custardCreamButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Custard cream");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int check = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Check confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if(check == 0){
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is "+taxprice+" yen(including tax).");
                    order="";
                    textPane1.setText(order);
                    sum=0;
                    total0YenTextPane.setText("Total　"+sum+" yen(excluding tax)");
                    total0YenIncludingTextPane.setText("Total　"+taxprice+" yen(including tax)");
                    count = 0;
                    numberOfOrders0TextPane.setText("Number of Orders　"+count);
                    taxprice =0;
                    total0YenIncludingTextPane.setText("Total　"+taxprice+" yen(including tax)");
                    boolean status = takeOutCheckBox.isSelected();
                    status=false;
                    takeOutCheckBox.setSelected(false);
                }

            }
        });
        takeOutCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean status = takeOutCheckBox.isSelected();
                if(status){ //テイクアウトの場合
                    taxprice = (int)(sum * 1.08);
                    total0YenIncludingTextPane.setText("Total　"+taxprice+" yen(including tax)");
                }else{//イートインの場合
                    taxprice = (int)(sum * 1.1);
                    total0YenIncludingTextPane.setText("Total　"+taxprice+" yen(including tax)");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Food");
        frame.setContentPane(new Food().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    //合計金額を保持する
    int sum = 0;
    //注文数
    int count = 0;
    //注文内容の文字列
    String order="";
    //taxpriceは、税込み価格
    int taxprice =0;
    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+food+"?",
                "Order confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0){
            if(food == "Pon de Ring"){
                //注文内容を格納
                order+="Pon de Ring　110yen\n";
                sum+=110;
                count+=1;
            }
            if(food == "Old fashion"){
                order+="Old fashion　120yen\n";
                sum+=120;
                count+=1;
            }
            if(food == "French cruller"){
                order+="French cruller　130yen\n";
                sum+=130;
                count+=1;
            }
            if(food == "Strawberry ring"){
                order+="Strawberry ring　140yen\n";
                sum+=140;
                count+=1;
            }
            if(food == "Golden chocolate"){
                order+="Golden chocolate　150yen\n";
                sum+=150;
                count+=1;
            }
            if(food == "Custard cream"){
                order+="Custard cream　160yen\n";
                sum+=160;
                count+=1;
            }

            JOptionPane.showMessageDialog(null,"Thank you for ordering "+food+"! It will be served as soon as possible.");
        }

        textPane1.setText(order);
        total0YenTextPane.setText("Total　"+sum+" yen(excluding tax)");
        //初期値は、ラジオボタンにチェックが入っていないので、税率10%表示となる。
        taxprice = (int)(sum * 1.1);
        total0YenIncludingTextPane.setText("Total　"+taxprice+" yen(including tax)");
        numberOfOrders0TextPane.setText("Number of Orders　"+count);
    }
}